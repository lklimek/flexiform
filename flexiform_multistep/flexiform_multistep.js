(function ($) {

  Drupal.behaviors.flexiform_multistep_lock_on_submit = {
    attach: function (context, settings) {
      if (typeof settings.flexiform_multistep.ajax_wrapper !== 'undefined') {
        var wrapper = settings.flexiform_multistep.ajax_wrapper;

        $(document).ajaxStart(function () {
          $('#' + wrapper + ' input:not([readonly="readonly" ').attr('readonly', 'readonly').addClass('flexiform-multistep-readonly');
          $('#' + wrapper + ' textarea:not([readonly="readonly" ').attr('readonly', 'readonly').addClass('flexiform-multistep-readonly');
        });
        $(document).ajaxStop(function () {
          $('#' + wrapper + ' input.flexiform-multistep-readonly').removeAttr('readonly').removeClass('flexiform-multistep-readonly');
          $('#' + wrapper + ' textarea.flexiform-multistep-readonly').removeAttr('readonly').removeClass('flexiform-multistep-readonly');
        });
      }

      // if we don't have any scrollTo plugin, create it
      if (typeof jQuery.scrollTo == 'undefined') {
        jQuery.fn.extend({
          scrollTo: function (selector,context) {
            $('html, body').animate({
              scrollTop: $(selector,context).first().offset().top - 100
            }, 500);
          }
        });
      }

      // Add method to load content with Ajax
      jQuery.fn.extend(
        {
          flexiformLoadAjax: function(selector, url) {
            var element = $(selector).first();
            var base = element.attr('id');

            var element_settings = {
              url: url,
              event: 'click',
              progress: {
                type: 'throbber'
              }
            };
            Drupal.ajax[base] = new Drupal.ajax(base, element, element_settings);
            element.click();
          }
        }
      );
    }
  }
})(jQuery);
