Introduction
------------

The Flexiform module provides a UI and configuration storage for the creation
of complex forms including fields and elements from multiple entities.

Requirements
------------

Drupal 7.x
Dependencies:
 - Entity API
 - Views

Installation
------------

Install and enable the Flexiform module as usual.

Getting Started
---------------

To start building your forms visit admin/structure/flexiforms and click
'Add Flexible Form'.

Creating a Flexiform
--------------------

1. Visit admin/structure/flexiforms/add and enter the Title for your form.
2. Select a base entity type and base entity bundle using the two drop downs
   provided. The base entity is the main entity the form deals with, other
   entities can be added to the form later.
3. Select a builder to use to build the form. Flexiform ships with 2 builders
   as standard:
   - Flexiform Form Builder: This is the main component of flexiform. It
     allows you to add more entities and custom elements to your form.
   - Entity Field Form: This builder will render a form containing all of the
     Field API fields on the base entity and bundle. Adding entities and
     elements will have no effect when using this builder!
4. Enter the Path to the form. There are two path inputs:
   - Path to Form: Path to the form in 'create' mode, this will create a new
     base entity and build the form from there.
   - Path to Edit Form: Path to the form in 'edit' mode, this will load the
     base entity based on an argument in the path (denoted by a %).
5. Click 'Save Flexiform'. You can now add Entities and Elements to your form
   by clicking 'Manage Form Entities' and 'Manage Form Fields' respectively.

Flexiform multistep in CTools modals
------------------------------------

To display flexiform multistep in ctools modal window, you should proceed as following:

1. Ensure that the page you are viewing has Ajax and Modal libraries enabled; for example:

<?php
function MYMODULE_init() {
  ctools_include('ajax');
  ctools_include('modal');
  ctools_modal_add_js();
}
?>

2. Place a link with "use-ajax" class that points to form page with two slashes
and "modal" keyword appendend at the end, for example:
<a href='/FlexiFormPage//modal' class='use-ajax'>click here</a>

where '/FlexiFormPage' is an URL to flexiform page which should be displayed in modal window.

3. Enjoy the form :)

Flexiform multistep: load confirmation page with Ajax
-----------------------------------------------------

You can execute custom Ajax action on form completion, including loading of another flexiform.


To use this feature, you should:
* enter the URL in "Redirect on completion" field
* select "load redirect URL with AJAX" checkbox OR "Use redirect URL as source of AJAX commands"

"Load redirect URL with AJAX" will simply replace the form with raw HTML content downloaded
from the "redirect on completion" URL.

"Use redirect URL as source of AJAX commands" will execute commands returned after
making GET request to the "redirect on completion" URL. To use this feature to load
another form, use the URL like '/FlexiFormPage//modal' (to open modal window) or '/FlexiFormPage//ajax'
(to use ajaxified form).
